'use strict';

(() => {

    const listTemplate = `
        <li>
            <p>__name__ X __quantity__</p>
            <button class="decrease" data-id="__id__">-</button>
            <button class="increase" data-id="__id__">+</button>
        </li>
    `;

    const cartElem = document.getElementById('cart');

    const increase = id => {
        cart.increase(id);
        loadItems();
    }

    const decrease = id => {
        cart.decrease(id);
        loadItems();
    }

    const loadItems = () => {
        cartElem.innerHTML = '';
        for(let item of cart.items) {
            let product = DATA.products.find(x => x.id == item.productId);
            cartElem.innerHTML 
                += listTemplate
                    .replace('__name__', product.name)
                    .replace('__quantity__', item.quantity)
                    .replaceAll('__id__', item.productId);
        }

        let dButtons =  document.querySelectorAll('.decrease');
        for(let b of dButtons) {
            b.addEventListener('click', (e) => decrease(e.target.getAttribute('data-id')))
        }

        let iButtons =  document.querySelectorAll('.increase');
        for(let b of iButtons) {
            b.addEventListener('click', (e) => increase(e.target.getAttribute('data-id')))
        }
        
    }

    loadItems();
})();