'use strict';
(() => {
    const productsEl = document.getElementById('products');

    const listItemTemplate = `
        <li>
            <div class="card" data-id="__id__">
                <p>__name__</p>
                <img src="assets/images/__img__">
                <p>__price__€</p> 
            </div>
        </li>
    `; 

    const loadItems = () => {
        for(let p of DATA.products) {
            productsEl.innerHTML 
                += listItemTemplate
                .replace('__name__', p.name)
                .replace('__img__', p.image)
                .replace('__id__', p.id)
                .replace('__price__', p.price);
        }

        let items = document.querySelectorAll('.card');
        for(let card of items) {
            card.addEventListener('click', e => cart.add(e.target.getAttribute('data-id')))
        }
    }

    loadItems();

})()