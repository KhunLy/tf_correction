class Cart {
    constructor() {
        this.items = JSON.parse(localStorage.getItem('CART')) || [];
    }

    save = () => {
        let json = JSON.stringify(this.items);
        localStorage.setItem('CART', json);
    }

    add = (id) => {
        let cartItem = this.items.find(x => x.productId == id);
        if(!cartItem) {
            this.items.push({ productId: id, quantity: 1 })
        }
        else {
            cartItem.quantity++;
        }
        this.save();
    }

    increase = (id) => {
        let prod = this.items.find(x => x.productId == id);
        if(prod) {
            prod.quantity++;
        }
        this.save();
    }

    decrease = (id) => {
        let prod = this.items.find(x => x.productId == id);
        if(prod) {
            if(prod.quantity === 1) {
                let i  = this.items.indexOf(prod);
                this.items.splice(i, 1);
                //cart = cart.filter(x => x != prod);
            }
            else {
                prod.quantity--;
            }
        }
        this.save();
    }
}

const cart = new Cart();